pub mod constants;

use crate::constants::*;
use anyhow::{bail, Result};
use once_cell::sync::Lazy;
use regex::Regex;

fn parse_word(syllable: &str) -> Result<String> {
    let caps = ZHUYIN.captures(syllable).unwrap();

    if let Some(pinyin) = caps.name("pinyin") {
        return Ok(pinyin.as_str().to_string());
    }

    let syllable = match caps.name("syllable") {
        Some(s) => s.as_str(),
        None => match caps.name("syllable1") {
            Some(s1) => s1.as_str(),
            None => match caps.name("syllable5") {
                Some(s5) => s5.as_str(),
                None => bail!("No syllable found."),
            },
        },
    };

    let syllable = *ZHUYIN2PINYIN.get(syllable).unwrap();
    let syllable = syllable.to_string();
    let tone = match caps.name("tone") {
        Some(s) => s.as_str(),
        None => match caps.name("tone5") {
            Some(s5) => s5.as_str(),
            None => "",
        },
    };

    let tone = *TONE2PINYIN.get(tone).unwrap();

    let add_r = match caps.name("er") {
        Some(er) => {
            if er.as_str() == "ㄦ" {
                "r".to_string()
            } else {
                "".to_string()
            }
        }
        None => match caps.name("er1") {
            Some(er) => {
                if er.as_str() == "ㄦ" {
                    "r".to_string()
                } else {
                    "".to_string()
                }
            }
            None => match caps.name("er5") {
                Some(er) => {
                    if er.as_str() == "ㄦ" {
                        "r".to_string()
                    } else {
                        "".to_string()
                    }
                }
                None => "".to_string(),
            },
        },
    };
    Ok(format!("{}{}{}", syllable, tone, add_r))
}

pub fn parse_full(zhuyin: &str) -> Result<String> {
    let syllables: Vec<String> = zhuyin
        .split('\u{3000}')
        .filter(|e| !e.is_empty())
        .map(|s| s.to_string())
        .collect();

    let parsed = syllables
        .iter()
        .map(|w| {
            COMMA
                .split(w)
                .map(|s| parse_word(s).unwrap())
                .collect::<Vec<String>>()
                .join(", ")
        })
        .collect::<String>();

    Ok(parsed)
}

static COMMA: Lazy<Regex> = Lazy::new(|| Regex::new(r"[，﹐]").unwrap());

static ZHUYIN: Lazy<Regex> = Lazy::new(|| {
    Regex::new(
        r"(?x)
        ^
        (
            (?P<syllable>[ㄅ-ㄩ]+)
            (?P<tone>[ˊˇˋ])
            (?P<er>ㄦ?)
        )
        $
        |
        ^
        (
            (?P<syllable1>[ㄅ-ㄩ]+?)
            (?P<er1>ㄦ?)
        )
        $
        |
        ^
        (
            (?P<tone5>˙)
            (?P<syllable5>[ㄅ-ㄩ]+?)
            (?P<er5>ㄦ?)
        )
        $
        |
        ^
        (
            (?P<pinyin>[a-z1-4]+)
        )
        $",
    )
    .unwrap()
});

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn zhuyin() {
        let zhuang = "ㄓㄨㄤ";
        let zhuang_good = "zhuang";
        let answer = *ZHUYIN2PINYIN.get(zhuang).unwrap();
        assert_eq!(answer, zhuang_good);
    }

    #[test]
    fn tones() {
        let three = "ˇ";
        let three_good = "3";
        let answer = *TONE2PINYIN.get(three).unwrap();
        assert_eq!(answer, three_good);
    }

    #[test]
    fn full() {
        let zhuang3 = "ㄓㄨㄤˇ";
        let zhuang3_good = "zhuang3";
        let answer = parse_full(zhuang3).unwrap();
        assert_eq!(answer, zhuang3_good);

        let kuang1 = "ㄎㄨㄤ";
        let kuang1_good = "kuang1";
        let answer = parse_full(kuang1).unwrap();
        assert_eq!(answer, kuang1_good);

        let diar = "ㄧˋ　ㄉㄧㄥ　ㄉㄧㄚˇㄦ";
        let diar_good = "yi4ding1dia3r";
        let answer = parse_full(diar).unwrap();
        assert_eq!(answer, diar_good);

        let xia = "ㄧˊ　ㄒㄧㄚˋ　˙ㄗ";
        let xia_good = "yi2xia4zi";
        let answer = parse_full(xia).unwrap();
        assert_eq!(answer, xia_good);

        let hur = "ㄧˋ　ㄏㄨㄦ";
        let hur_good = "yi4hu1r";
        let answer = parse_full(hur).unwrap();
        assert_eq!(answer, hur_good);

        let huer = "ㄏㄨㄜˇㄦ";
        let huer_good = "hue3r";
        let answer = parse_full(huer).unwrap();
        assert_eq!(answer, huer_good);

        let jier = "ㄐㄧㄜˊㄦ";
        let jier_good = "jie2r";
        let answer = parse_full(jier).unwrap();
        assert_eq!(answer, jier_good);

        let ber = "ㄅㄜˋㄦ";
        let ber_good = "be4r";
        let answer = parse_full(ber).unwrap();
        assert_eq!(answer, ber_good);

        let wer = "ㄨㄜˋㄦ";
        let wer_good = "we4r";
        let answer = parse_full(wer).unwrap();
        assert_eq!(answer, wer_good);

        let er2 = "ㄦˊ";
        let er2_good = "er2";
        let answer = parse_full(er2).unwrap();
        assert_eq!(answer, er2_good);

        let er2 = "ㄦˊ　˙ㄗ";
        let er2_good = "er2zi";
        let answer = parse_full(er2).unwrap();
        assert_eq!(answer, er2_good);

        let er2 = "ㄋㄩˇ　ㄦˊ";
        let er2_good = "nü3er2";
        let answer = parse_full(er2).unwrap();
        assert_eq!(answer, er2_good);

        let yu4 = "ㄅㄨˋ　ㄧㄢˊ　ㄎㄜˇ　　ㄩˋ";
        let yu4_good = "bu4yan2ke3yu4";
        let answer = parse_full(yu4).unwrap();
        assert_eq!(answer, yu4_good);

        let fer4 = "ㄈㄜˋㄦ";
        let fer4_good = "fe4r";
        let answer = parse_full(fer4).unwrap();
        assert_eq!(answer, fer4_good);

        let comma1 = "ㄗㄨㄛˋ，ㄦˋ";
        let comma1_good = "zuo4, er4";
        let answer = parse_full(comma1).unwrap();
        assert_eq!(answer, comma1_good);

        let comma2 = "ㄌㄟˊ﹐ㄅㄨˊ";
        let comma2_good = "lei2, bu2";
        let answer = parse_full(comma2).unwrap();
        assert_eq!(answer, comma2_good);

        let duer = "ㄉㄨㄜㄦ";
        let duer_good = "due1r";
        let answer = parse_full(duer).unwrap();
        assert_eq!(answer, duer_good);

        let biar = "ㄅㄧㄚㄦ";
        let biar_good = "bia1r";
        let answer = parse_full(biar).unwrap();
        assert_eq!(answer, biar_good);

        let piar = "ㄆㄧㄚˋㄦ";
        let piar_good = "pia4r";
        let answer = parse_full(piar).unwrap();
        assert_eq!(answer, piar_good);

        let guer = "ㄍㄨㄜˋㄦ";
        let guer_good = "gue4r";
        let answer = parse_full(guer).unwrap();
        assert_eq!(answer, guer_good);

        let quar = "ㄑㄩㄚㄦ";
        let quar_good = "qua1r";
        let answer = parse_full(quar).unwrap();
        assert_eq!(answer, quar_good);

        let yuar = "ㄩㄚˊㄦ";
        let yuar_good = "yua2r";
        let answer = parse_full(yuar).unwrap();
        assert_eq!(answer, yuar_good);

        let dier = "ㄉㄧㄜˇㄦ";
        let dier_good = "die3r";
        let answer = parse_full(dier).unwrap();
        assert_eq!(answer, dier_good);

        let pier = "ㄆㄧㄜˊㄦ";
        let pier_good = "pie2r";
        let answer = parse_full(pier).unwrap();
        assert_eq!(answer, pier_good);

        let yai = "ㄧㄞˊ";
        let yai_good = "yai2";
        let answer = parse_full(yai).unwrap();
        assert_eq!(answer, yai_good);

        let hahar = "ㄏㄚ\u{3000}˙ㄏㄚㄦ";
        let hahar_good = "ha1har";
        let answer = parse_full(hahar).unwrap();
        assert_eq!(answer, hahar_good);

        let yo = "ㄏㄥ\u{3000}ㄧㄛ";
        let yo_good = "heng1yo1";
        let answer = parse_full(yo).unwrap();
        assert_eq!(answer, yo_good);

        let zuer = "ㄗㄨㄜˇㄦ";
        let zuer_good = "zue3r";
        let answer = parse_full(zuer).unwrap();
        assert_eq!(answer, zuer_good);

        let miar = "ㄇㄧㄚˋㄦ";
        let miar_good = "mia4r";
        let answer = parse_full(miar).unwrap();
        assert_eq!(answer, miar_good);

        let quer = "ㄑㄩㄜˇㄦ";
        let quer_good = "que3r";
        let answer = parse_full(quer).unwrap();
        assert_eq!(answer, quer_good);

        let yer = "ㄧㄜˋㄦ";
        let yer_good = "ye4r";
        let answer = parse_full(yer).unwrap();
        assert_eq!(answer, yer_good);

        let zhuer = "ㄓㄨㄜˇㄦ";
        let zhuer_good = "zhue3r";
        let answer = parse_full(zhuer).unwrap();
        assert_eq!(answer, zhuer_good);

        let lüan = "ㄌㄩㄢˊ";
        let lüan_good = "lüan2";
        let answer = parse_full(lüan).unwrap();
        assert_eq!(answer, lüan_good);

        let juar = "ㄐㄩㄚˇㄦ";
        let juar_good = "jua3r";
        let answer = parse_full(juar).unwrap();
        assert_eq!(answer, juar_good);

        let ei = "ㄝˋ";
        let ei_good = "ei4";
        let answer = parse_full(ei).unwrap();
        assert_eq!(answer, ei_good);

        let mie = "ㄇㄧㄜˊㄦ";
        let mie_good = "mie2r";
        let answer = parse_full(mie).unwrap();
        assert_eq!(answer, mie_good);

        let xie = "ㄒㄧㄜˋㄦ";
        let xie_good = "xie4r";
        let answer = parse_full(xie).unwrap();
        assert_eq!(answer, xie_good);

        let bie = "ㄅㄧㄜˊㄦ";
        let bie_good = "bie2r";
        let answer = parse_full(bie).unwrap();
        assert_eq!(answer, bie_good);
    }
}
