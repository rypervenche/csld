// TODO: Use Moedict for any words not found
// TODO: Maybe separate out into lib
// TODO: Maybe add english as english meaning?
use anyhow::{bail, Result};
use once_cell::sync::Lazy;
use regex::Regex;
use serde::{Deserialize, Serialize};
use zhuyin_pinyin::*;

const DICT: &str = include_str!("../resources/dict-csld.json");
const TOCFL: &str = include_str!("../resources/國教院三等七級詞表.csv");

#[derive(Serialize, Deserialize, Clone, Debug)]
struct Word {
    heteronyms: Vec<Variant>,
    non_radical_stroke_count: Option<usize>,
    radical: Option<String>,
    stroke_count: Option<usize>,
    title: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
struct Variant {
    alt: Option<String>,
    bopomofo: String,
    definitions: Vec<Definition>,
    id: String,
    pinyin: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
struct Definition {
    def: String,
    example: Option<Vec<String>>,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
struct CsldWord {
    chs: String,
    cht: String,
    pinyin: Vec<String>,
    zhuyin: String,
    translation: Vec<String>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(untagged)]
enum Band {
    第1級 = 1,
    第2級,
    第3級,
    第4級,
    第5級,
    第6級,
    第7級,
}

impl Band {
    fn new(string: &str) -> Result<Self> {
        match string {
            "第1級" => Ok(Band::第1級),
            "第2級" => Ok(Band::第2級),
            "第3級" => Ok(Band::第3級),
            "第4級" => Ok(Band::第4級),
            "第5級" => Ok(Band::第5級),
            "第6級" => Ok(Band::第6級),
            "第7級" => Ok(Band::第7級),
            _ => bail!("Bad input for enum."),
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
struct TocflWord {
    band: Band,
    hanzi: String,
    zhuyin: String,
    english: String,
}

impl TocflWord {
    fn new(vec: Vec<&str>) -> Result<Self> {
        let band = Band::new(vec.first().unwrap())?;
        let hanzi = vec.get(1).unwrap().replace(char::is_numeric, "");
        let english = vec.get(3).unwrap().replace('\"', "");
        Ok(TocflWord {
            band,
            hanzi,
            zhuyin: vec.get(2).unwrap().to_string(),
            english,
        })
    }
}

static UNNEEDED: Lazy<Regex> = Lazy::new(|| {
    Regex::new(
        r"(?x)
            (?:<br>)
            |
            (?:\u{f8f8})
            |
            (?:[臺陸][\u{20df}\u{20dd}]) # Enclosing diamond and circle
        ",
    )
    .unwrap()
});

static CIRCLE: Lazy<Regex> = Lazy::new(|| {
    Regex::new(
        r"(?x)
            (\p{Han})[\u{20df}\u{20dd}] # Enclosing diamond and circle
        ",
    )
    .unwrap()
});

static FONT: Lazy<Regex> = Lazy::new(|| {
    Regex::new(
        r"(?x)
            (:?<font[^>]+>)([^<]+)(:?<[^>]+>) # <font face=...>5̣</font>
        ",
    )
    .unwrap()
});

static BAGUA: Lazy<Regex> = Lazy::new(|| {
    Regex::new(
        r"(?x)
            (:?\n<table.*) # HTML table
        ",
    )
    .unwrap()
});

static PARENS: Lazy<Regex> = Lazy::new(|| {
    Regex::new(
        r"(?x)
            ^
            ([^\(]*)   # Before
            \([^\)]+\) # Parentheses and word inside
            (.*)       # After
            $
        ",
    )
    .unwrap()
});

fn get_tocfl() -> Result<Vec<TocflWord>> {
    let vec: Vec<TocflWord> = TOCFL
        .lines()
        .skip(1)
        .map(str::trim)
        .filter(|&l| !l.is_empty() && l != "\"")
        .map(|l| l.split(',').collect::<Vec<&str>>())
        .collect::<Vec<Vec<&str>>>()
        .iter()
        .map(|v| TocflWord::new(v.clone()).unwrap())
        .collect();
    Ok(vec)
}

fn tocfl_in_csld<'a>(csld: &'a [CsldWord], word: &str, zhuyin: &str) -> Option<&'a CsldWord> {
    if !zhuyin.is_empty() {
        match csld.iter().find(|c| c.cht == word && c.zhuyin == zhuyin) {
            Some(csld_word) => Some(csld_word),
            None => csld.iter().find(|c| c.cht == word),
        }
    } else {
        csld.iter().find(|c| c.cht == word)
    }
}

fn merge_lists(
    csld: &[CsldWord],
    tocfl: Vec<TocflWord>,
) -> Result<(Vec<Vec<&CsldWord>>, Vec<String>)> {
    let mut merged = vec![vec![], vec![], vec![], vec![], vec![], vec![], vec![]];
    let mut not_found = vec![];
    for word in tocfl {
        let hanzi = word.hanzi.trim();
        let zhuyin = word.zhuyin;
        let band_index = word.band.clone() as usize - 1;
        let mut hanzi_variants = vec![hanzi.to_string()];
        for hanzi in hanzi_variants.clone() {
            if hanzi.contains('/') {
                hanzi_variants = hanzi.split('/').map(str::to_string).collect();
                for hanzi_variant in hanzi_variants.clone() {
                    match tocfl_in_csld(csld, &hanzi_variant, &zhuyin) {
                        Some(csld_word) => merged[band_index].push(csld_word),
                        None => continue,
                    }
                }
            }
        }
        if hanzi.contains('(') {
            let remove_parens = hanzi.replace('(', "");
            let remove_parens = remove_parens.replace(')', "");
            let remove_optional = PARENS.replace(hanzi, "$1$2").to_string();
            hanzi_variants.push(remove_parens);
            hanzi_variants.push(remove_optional);
            for hanzi_variant in hanzi_variants {
                match tocfl_in_csld(csld, &hanzi_variant, &zhuyin) {
                    Some(csld_word) => merged[band_index].push(csld_word),
                    None => continue,
                }
            }
            if hanzi.contains('台') {
                hanzi_variants = vec!["臺", "檯", "颱"]
                    .iter()
                    .map(|&t| hanzi.replace('台', t))
                    .collect();
                for hanzi_variant in hanzi_variants {
                    match tocfl_in_csld(csld, &hanzi_variant, &zhuyin) {
                        Some(csld_word) => merged[band_index].push(csld_word),
                        None => not_found.push(hanzi_variant.to_owned()),
                    }
                }
            }
        } else {
            match tocfl_in_csld(csld, hanzi, &zhuyin) {
                Some(csld_word) => merged[band_index].push(csld_word),
                None => not_found.push(hanzi.to_owned()),
            }
        }
    }
    Ok((merged, not_found))
}

fn get_csld() -> Result<Vec<CsldWord>> {
    let vec: Vec<Word> = serde_json::from_str(DICT)?;
    let csld: Vec<CsldWord> = vec
        .iter()
        .flat_map(|w| {
            w.heteronyms
                .iter()
                .map(|v| {
                    let simp = match &v.alt {
                        Some(s) => s,
                        None => &w.title,
                    };
                    let pinyin = UNNEEDED
                        .split(&v.pinyin)
                        .collect::<Vec<&str>>().first()
                        .unwrap()
                        .to_string();

                    let zhuyin = UNNEEDED
                        .split(&v.bopomofo)
                        .collect::<Vec<&str>>().first()
                        .unwrap()
                        .to_string();

                    let zhuyin = &zhuyin.replace('｜', "ㄧ");
                    let zhuyin = &zhuyin.replace(' ', "\u{3000}");
                    let zhuyin = &zhuyin.replace("ㄅㄚ˙", "˙ㄅㄚ");
                    let pinyin_diacritics = pinyin.replace('-', "");
                    let pinyin_diacritics = pinyin_diacritics.replace(' ', "");
                    let pinyin_numbers = parse_full(zhuyin).unwrap();
                    let pinyin_numbers = pinyin_numbers.replace('\u{3000}', "");
                    let pinyin: Vec<String> = vec![pinyin_numbers, pinyin_diacritics];
                    let zhuyin = zhuyin.replace('\u{3000}', "");

                    let translation: Vec<String> = v
                        .definitions
                        .iter()
                        .map(|d| {
                            let def = CIRCLE.replace_all(&d.def, "$1: ");
                            let def = FONT.replace_all(&def, "");
                            BAGUA.replace_all(&def, "").to_string()
                        })
                        .collect();
                    CsldWord {
                        chs: simp.to_string(),
                        cht: w.title.to_string(),
                        pinyin,
                        zhuyin,
                        translation,
                    }
                })
                .collect::<Vec<CsldWord>>()
        })
        .collect();
    Ok(csld)
}

fn main() -> Result<()> {
    let csld = get_csld()?;
    let tocfl = get_tocfl()?;

    let (mut merged, not_found) = merge_lists(&csld, tocfl)?;
    for band in merged.iter_mut() {
        band.dedup();
    }

    for (i, band) in merged.iter().enumerate() {
        let tocfl = serde_json::to_string(&band)?;
        let tocfl_pretty = serde_json::to_string_pretty(&band)?;
        let path = format!("/tmp/tocfl{}.json", i + 1);
        let path_pretty = format!("/tmp/tocfl{}_pretty.json", i + 1);
        std::fs::write(path, &tocfl).expect("Failed to write to file.");
        std::fs::write(path_pretty, &tocfl_pretty).expect("Failed to write to file.");
    }

    let not_found_pretty = serde_json::to_string_pretty(&not_found)?;
    std::fs::write("/tmp/not_found_pretty.json", not_found_pretty)
        .expect("Failed to write to file.");

    Ok(())
}
#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn parens_remove_inside() {
        let input1 = "一點(兒)";
        let input2 = "沒(有)用";
        let good1 = "一點";
        let good2 = "沒用";
        let answer1 = PARENS.replace(input1, "$1$2");
        let answer2 = PARENS.replace(input2, "$1$2");

        assert_eq!(good1, answer1);
        assert_eq!(good2, answer2);
    }
}
